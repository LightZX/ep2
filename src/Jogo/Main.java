package Jogo;

import java.awt.Dimension;
import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		JFrame janela = new JFrame("Jogo da Cobrinha");
		janela.setContentPane(new TelaJogo());
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setResizable(false);
		janela.pack();
		janela.setPreferredSize(new Dimension(TelaJogo.LARGURA, TelaJogo.ALTURA));
		janela.setLocationRelativeTo(null);
		janela.setVisible(true);
	}

}
